import requests, json
import hmac, hashlib, base64

class iq_client():
    """Client for iQuick API service."""

    PUBLIC_KEY = 'ecc1172212968db2c4881ba31c349dae51b00173c09b221cd00960abc00bceb8'
    SECRET_KEY = 'b0ef7f736fe0d45e060f367353811915d55aca1be634439c803374e6825bb42f'
    API_URL = 'https://iquick.io/api/'

    def post_request(self, method, data):
        req = requests.post(self.API_URL + method, data = data)
        req = req.json()
        if req['success']:
            return req['data']

        else:
            return req['message']

    def get_request(self, method):
        req = requests.get(self.API_URL + method)
        req = req.json()
        if req['success']:
            return req['data']

        else:
            return req['message']

    def GetTicker(self):
        return self.get_request('get_ticker')

    def CreateProduct(self, currencies, name, price, paytime):
        '''
            Method for create product on iQuick.

            !!! IMPORTANT !!!
            -    currencies = 'btc',
            -    currencies = 'dsh',
            -    currencies = 'eth',
            -    currencies = 'btcxeth',
            -    currencies = 'btcxdsh',
            -    currencies = 'btcxethxdsh',
            etc.
        '''

        #######################################################
        ### CREATE API SIGN ( HMAC_SHA256 on field "name" ) ###
        #######################################################

        digest = hmac.new(self.SECRET_KEY.encode('utf-8'), msg=name.encode('utf-8'), digestmod=hashlib.sha256).digest()
        api_sign = base64.b64encode(digest).decode()

        #################
        ### POST data ###
        #################

        data = {
            'name': name,
            'price': price,
            'paytime': paytime,
            'api_key': self.PUBLIC_KEY,
            'api_sign': api_sign,
        }

        return self.post_request('create/' + currencies + '/', data)

    def GetProduct(self, id_product):
        '''
            Method for get information about product
        '''

        return self.get_request('product_info/' + str(id_product))

    def ChangeProduct(self, id_product, name = None, about = None, price = None, paytime = None):
        '''
            Method for add and replace information
            for product.
        '''

        #######################################################
        ### CREATE API SIGN ( HMAC_SHA256 on field "name" ) ###
        #######################################################

        digest = hmac.new(self.SECRET_KEY.encode('utf-8'), msg=str(id_product).encode('utf-8'), digestmod=hashlib.sha256).digest()
        api_sign = base64.b64encode(digest).decode()

        #################
        ### POST data ###
        #################

        data = {
            'api_key': self.PUBLIC_KEY,
            'api_sign': api_sign,
        }

        if name:
            data.update({'name': name})
        if about:
            data.update({'about': about})
        if paytime:
            data.update({'paytime': paytime})
        if price:
            data.update({'price': price})

        return self.post_request('edit_product/' + id_product + '/', data)

    def MakeInvoice(self, currency, callback_url):
        '''
            Method for create quick payment and get invoice.

            !!! IMPORTANT !!!
            -    currency = 'btc' or 'dsh' or 'eth',
            -   callback_url = 'http://example.com/callback_page'
        '''

        #######################################################
        ### CREATE API SIGN ( HMAC_SHA256 on field "name" ) ###
        #######################################################

        digest = hmac.new(self.SECRET_KEY.encode('utf-8'), msg=callback_url.encode('utf-8'), digestmod=hashlib.sha256).digest()
        api_sign = base64.b64encode(digest).decode()

        #################
        ### POST data ###
        #################

        data = {
            'callback_url': callback_url,
            'api_key': self.PUBLIC_KEY,
            'api_sign': api_sign,
        }

        return self.post_request('payment/' + currency + '/', data)

def check():
    client = iq_client()
    print(client.get_ticker())
    print(client.CreateProduct('btcxeth', 'New product', 1, 0))
    print(client.GetProduct('11'))
    print(client.ChangeProduct('11', about='simple desc'))
    print(client.MakeInvoice('btc','http://example.com/callback_page'))
